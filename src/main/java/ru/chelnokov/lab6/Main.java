package ru.chelnokov.lab6;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Document doc =
                null;
        try {
            doc = Jsoup.connect("https://www.gismeteo.ru/weather-penza-4445/now/").get();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Нет интернета");
        }
        assert doc != null;

        System.out.println("Сейчас: " + doc.select("div [class=now-localdate]").text());
        System.out.println(doc.select("div [class=now-desc]").text());
        System.out.println("Температура: " + doc.select("div [class=unit unit_temperature_c]").first().text());
        System.out.println("Ветер: " + doc.select("div [class=unit unit_wind_m_s]").first().text());
        System.out.println(doc.select("div [class=now-info-item humidity]").first().text());


    }
}